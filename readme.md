
# How to install plugins
* go to plugins folder:
	
	cd /usr/src/redmine/plugins
	
	git clone https://gitlab.com/SothyLorn/redmine_plugins.git
* move all plugins outside redmine_plugin folder:
	
	mv redmine_* /usr/src/redmine/plugins/
* go to redmine folder:

	cd /usr/src/redmine/

* Run this command:
	
	bundle install --without development test

	bundle exec rake redmine:plugins NAME=redmine_agile RAILS_ENV=production

	bundle exec rake redmine:plugins NAME=redmine_checklists RAILS_ENV=production

	bundle exec rake redmine:plugins NAME=redmine_contacts RAILS_ENV=production

	bundle exec rake redmine:plugins NAME=redmine_contacts_invoices RAILS_ENV=production

	bundle exec rake redmine:plugins NAME=redmine_favorite_projects RAILS_ENV=production

	bundle exec rake redmine:plugins NAME=redmine_people RAILS_ENV=production

	bundle exec rake redmine:plugins NAME=redmine_products RAILS_ENV=production

	bundle exec rake redmine:plugins NAME=redmine_questions RAILS_ENV=production

	bundle exec rake redmine:plugins NAME=redmine_zenedit RAILS_ENV=production
	
	bundle exec rake redmine:plugins NAME=redmine_tags RAILS_ENV=production


